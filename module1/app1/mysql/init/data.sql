CREATE TABLE client
(
    id INT PRIMARY KEY NOT NULL,
    nom VARCHAR(100),
    prenom VARCHAR(100),
    tel VARCHAR(100),
    fax VARCHAR(255),
)
