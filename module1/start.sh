!/bin/bash

docker network create --subnet "172.18.0.0/16" reverseproxy
docker network create --subnet "172.19.0.0/16" app1
docker network create --subnet "172.20.0.0/16" app2

cd app1
docker-compose up -d
cd ..
cd app2
docker-compose up -d
cd ..
docker-compose up -d
